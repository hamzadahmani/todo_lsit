import React ,{useEffect} from 'react';
import MainRoute from './component/Router';
import './App.css';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './component/store/store';
import {Login} from './component/store/actions/authenticationAction';
import { useDispatch } from "react-redux";



function App() {
 
  
  return (
    <Provider store={store}>
      <BrowserRouter>
        <MainRoute />
      </BrowserRouter>
    </Provider>
  );
}

export default App;
