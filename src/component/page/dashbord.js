import React, { useState } from "react";
import TasksAccordions from "../layout/TaskAccordion";
import AppBarMenu from "../layout/appBar";
import AddTask from "../layout/addTask";
import Container from '@material-ui/core/Container';
import { useSelector } from "react-redux";
import { Typography } from "@material-ui/core";
import { useDispatch } from "react-redux";
import { SnackData } from "../store/actions/taskAction";
import Snack from "../layout/snackBar";

const Dashbord = () => {
    const taskList = useSelector(state => state.TaskList.Tasks)
    const SnackObject = useSelector(state => state.TaskList.Snack)
    const dispatch = useDispatch()

    return (
        <>
            <AppBarMenu />
            <div style={{ display: "flex", justifyContent: "center", marginTop: 20 }}>
                <Snack status={SnackObject.open} message={SnackObject.message} type={SnackObject.type}
                    Close={(value) => dispatch(SnackData({
                        open: value,
                        message: '',
                        type: ''
                    }))} />
                {console.log('SnackObject', SnackObject)}
                <Container>
                    <Typography variant="h5">
                        Tasks list
               </Typography>

                    {
                        taskList.length ?
                            taskList.map((el, index) => {
                                return <TasksAccordions index={index} key={index} name={el.name} details={el.details} status={el.status} data={el} />
                            }) :
                            <Typography style={{ color: "red" }}>
                                empty tasks ! please add new task
                     </Typography>
                    }

                    <AddTask />
                </Container>
            </div>
        </>
    );
};
export default Dashbord;
