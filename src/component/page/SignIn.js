import React, { useState,useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
 import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useDispatch } from "react-redux";
import { Login } from "../store/actions/authenticationAction";
import { handel_task } from '../store/actions/taskAction';


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://www.linkedin.com/in/dahmani-hamza-46b22a177/">
        Hamza Dahmani
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',  
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignIn() {
  const classes = useStyles();
  const [email, setEmail] = useState()
  const [error, setError] = useState(false)
  const [password, setPassword] = useState()
  const dispatch = useDispatch()

 //check   localStorage
  useEffect(()=>{
    const User = localStorage.getItem('User');
    const UserDataTasks = localStorage.getItem('tasks');
    const data=JSON.parse(User)
    const usertasks=JSON.parse(UserDataTasks)
    if (data){
      dispatch(Login(data))
    }
    dispatch(handel_task(usertasks))
  },[])
//if email and password are correct user redrected to home page and save her info in localStorage
  const onSubmit = () => {
     if (email === "test@test.com" && password === "test") {
      const data = {
        email: email,
        password: password,
        login:true
      };
      setError(false)
      localStorage.setItem('User', JSON.stringify(data));
      dispatch(Login(data))
    }else{
      setError(true)
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={(event) => { setEmail(event.target.value) }}
      
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={(event) => { setPassword(event.target.value) }}

          />
       {error && 
             <Typography  style={{ color: "red" }}  >
            email or password incorrect !
           </Typography>
       }
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={() => onSubmit()}
          >
            Sign In
          </Button>
       
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}