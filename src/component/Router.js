import React, { useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import { useSelector } from "react-redux";
import SignIn from './page/SignIn'
import Dashbord from './page/dashbord'
import NotFound from "./page/notFound";
 

export default function MainRoute() {
  
 
  return (
    <Router>
        <Switch>
        <PublicRoute exact path="/">
            <SignIn />
          </PublicRoute>
          <PrivateRoute   path="/Dashbord">
            <Dashbord />
          </PrivateRoute>
          <Route path="*">
          <NotFound />
        </Route>

        </Switch>
    </Router>
  );
}

function PublicRoute({ children, ...rest }) {
    const userData = useSelector(state => state.authentication.userData.login)
    return (
      <Route
        {...rest}
        render={({ location }) =>
          userData  ? (
            <Redirect
              to={{
                pathname: "/Dashbord",
                state: { from: location }
              }}
            />
          ) : (
              children
            )
        }
      />
    );
  }
  
  /** 
   * A wrapper for <Route> that redirects to the login
   * screen if you're not yet authenticated.
  */
  function PrivateRoute({ children, ...rest }) {
    const userData = useSelector(state => state.authentication.userData.login)
    return (
      <Route
        {...rest}
        render={({ location }) =>
           userData ? (
            children
          ) : (
              <Redirect
                to={{
                  pathname: "/",
                  state: { from: location }
                }}
              />
            )
        }
      />
    );
  }