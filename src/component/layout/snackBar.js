import React from "react";
import MuiAlert from '@material-ui/lab/Alert';
import { Snackbar } from '@material-ui/core';

const Snack = (props) => {
    function Alert(props) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
    }
    
    return (
     
     <Snackbar  anchorOrigin={{  vertical: 'top', horizontal: 'center'}} open={props.status} autoHideDuration={6000} onClose={() =>props.Close(false)}>
                <Alert onClose={() => props.Close(false)} severity={props.type}>
                    {props.message}
                </Alert>
            </Snackbar>
     
    );
};
export default Snack;
