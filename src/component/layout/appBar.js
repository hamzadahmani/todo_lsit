import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
 import { useDispatch, useSelector } from "react-redux";
import { Login } from "../store/actions/authenticationAction";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

export default function  AppBarMenu(props) {
  const classes = useStyles();
  const dispatch = useDispatch()

  const logout=()=>{
    localStorage.removeItem('User');
    dispatch(Login({}))
  }
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            Home
          </Typography>
          <Button onClick={()=>logout()} color="inherit">LOGOUT</Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}