import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { TextField } from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import SaveIcon from '@material-ui/icons/Save';
import { update_task, deleteTask } from '../store/actions/taskAction';
import { useDispatch } from 'react-redux';
 

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        margin: 10
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
    nameTask: {
        display: "flex",
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        alignItems: 'center'

    },
    progress: {
        fontSize: theme.typography.pxToRem(13),
        color: 'white',
        borderRadius: '5px',
        padding: '3px',
        textAlign: 'center',
        minWidth: 60,
    },
    AccordionDetails: {
        flexDirection: 'column'
    },

    DetailsTask: {
        display: "flex",
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '70%',
        alignItems: 'center'

    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },

}));
 
export default function ControlledAccordions(props) {
    const classes = useStyles();
    const [expanded, setExpanded] = useState(false);
    const [taskName, setTaskName] = useState()
    const [taskDetails, setTaskDetails] = useState()
    const [status, setStatus] = useState(props.status);
    const [open, setOpen] = useState(false);
    const [error, setError] = useState(false)

    const disapatch = useDispatch()
    useEffect(() => {
        setTaskName(props.name)
        setTaskDetails(props.details)
        setStatus(props.status)
    }, [props.name, props.details, props.status])

    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };


    const handleChangeStatus = (event) => {
        setStatus(event.target.value);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleOpen = () => {
        setOpen(true);
    };
    const updateTask = () => {

        const Task = {
            name: taskName,
            details: taskDetails,
            status: status
        }
        if (taskName && taskDetails && taskName.trim() != '' && taskDetails.trim() != '') {
            setError(false)
            disapatch(update_task(Task, props.index))

        } else {
            setError(true)
        }



    };
    const deleteOneTask = () => {
        disapatch(deleteTask(props.index))
    };



    return (
        <div className={classes.root}>

            <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1bh-content"
                    id="panel1bh-header"
                >
                    <div className={classes.nameTask}>
                        <div>
                            <Typography className={classes.heading}>{props.name}</Typography>
                            <Typography className={classes.secondaryHeading}>{props.details}</Typography>
                        </div>
                        <div>
                            <Typography className={classes.progress} style={{ backgroundColor: props.status == 0 ? "red" : props.status == 1 ? "#eed202" : 'green' }}>{
                                props.status == 0 ? "new" : props.status == 1 ? "in Progress" : 'done'
                            }</Typography>
                        </div>
                    </div>
                </AccordionSummary>
                <AccordionDetails className={classes.AccordionDetails}>
                    <Typography>
                        update Details
                    </Typography>
                    <div className={classes.DetailsTask}>
                        <TextField onChange={(event) => { setTaskName(event.target.value) }} value={taskName} margin="dense" id="outlined-basic1" label="Task Name" variant="outlined" />
                        <TextField onChange={(event) => { setTaskDetails(event.target.value) }} value={taskDetails} margin="dense" id="outlined-basic2" label="Task Details" variant="outlined" />
                        <FormControl className={classes.formControl}>
                            <InputLabel id="demo-controlled-open-select-label">status</InputLabel>
                            <Select
                                labelId="demo-controlled-open-select-label"
                                id="demo-controlled-open-select"
                                open={open}
                                onClose={handleClose}
                                onOpen={handleOpen}
                                value={status}
                                onChange={handleChangeStatus}
                            >

                                <MenuItem value={0}>new</MenuItem>
                                <MenuItem value={1}>in progress</MenuItem>
                                <MenuItem value={2}>done</MenuItem>
                            </Select>
                        </FormControl>

                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.button}
                            endIcon={<SaveIcon />}
                            onClick={() => updateTask()}
                        >
                            save
                        </Button>
                        <Button
                            variant="contained"
                            color="secondary"
                            className={classes.button}
                            startIcon={<DeleteIcon />}
                            onClick={() => deleteOneTask()}
                        >
                            Delete
                        </Button>
                    </div>
                    {error &&
                        <Typography style={{ color: "red" }}>
                            all Fileds are required !
                        </Typography>}
                </AccordionDetails>

            </Accordion>

        </div>
    );
}