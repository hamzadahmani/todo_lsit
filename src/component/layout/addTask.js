import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import {  TextField } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import { FormControl } from '@material-ui/core';
import { addTask } from "../store/actions/taskAction";
import { useDispatch } from 'react-redux';
import  Snack  from "./snackBar";


const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        marginTop: '30px',
        marginBottom: "20px"
    },
}));




export default function AddTask() {
    const classes = useStyles();
    const [taskName, setTaskName] = useState()
    const [taskDetails, setTaskDetails] = useState()
    const [error, setError] = useState(false)
    const dispatch = useDispatch()
    const [open, setOpen] = useState(false);
    const saveTask = () => {
        const Newtask = {
            name: taskName,
            details: taskDetails,
            status: 0
        }
        if (taskName && taskDetails&&taskName.trim()!=''&&taskDetails.trim()!='' ) {
            setError(false)
            setTaskName('')
            setTaskDetails('')
            dispatch(addTask(Newtask))
            setOpen(true);
        } else {
            setError(true)
        }
 

     
    }
    return (

        <FormControl className={classes.root}>
        
            <Typography variant="h5"  >Add new task</Typography>
            <div style={{ display: "flex", justifyContent: 'space-around', alignItems: "center", width: "50%" }}>
                <TextField value={taskName} required margin="dense" id="outlined-basic" label="Task Name" variant="outlined" onChange={(event) => setTaskName(event.target.value)} />
                <TextField value={taskDetails} required margin="dense" id="outlined-basic" label="Task Details" variant="outlined" onChange={(event) => setTaskDetails(event.target.value)} />
                <Button
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    endIcon={<SaveIcon />}
                    onClick={() => saveTask()}
                >
                    save
        </Button>
            </div>
            {error &&
                <Typography style={{ color: "red" }}>
                    all Fileds are required !
        </Typography>}
       
        <Snack status={open} message={'success add'} type={"success"} Close={(value)=>setOpen(value)}/>
        </FormControl>

    );
}