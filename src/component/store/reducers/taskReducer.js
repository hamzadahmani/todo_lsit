import {ADD_TASK , UPDATE_TASK,DELETE_TASK ,GET_TASK,SNACK} from "../types";

const initialState = {
    Tasks: [],
    Snack:{
      open: false,
      message: '',
      type: ''
    }
};

const taskList = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TASK:
      return {
        ...state,
        Tasks:action.payload ,
      };
      case UPDATE_TASK:
      return {
        ...state,
        Tasks: action.payload ,
      };
      case DELETE_TASK:
        
        return {
          ...state,
          Tasks: action.payload,
        };
        case GET_TASK:
        
        return {
          ...state,
          Tasks: action.payload,
        };
        case SNACK:
           return {
            ...state,
            Snack: action.payload,
          };
    default:
      return state;
  }
};
export default taskList;