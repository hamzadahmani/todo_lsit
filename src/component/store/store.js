import {createStore, combineReducers,applyMiddleware } from "redux";
import authentication from './reducers/authenticationRedcer';
import TaskList from './reducers/taskReducer';
import thunk from 'redux-thunk';



export default createStore(
    combineReducers({
        authentication,
        TaskList
    }),
    {},  
    applyMiddleware(thunk)
);
