import {ADD_TASK , UPDATE_TASK,DELETE_TASK ,GET_TASK,SNACK} from "../types";

 
export const addTask = (data) => {
    return (dispatch,getstate)=> {
        let newArray = [data, ...getstate().TaskList.Tasks]
        dispatch(add_task(newArray))
        saveTaskInLocalStorage(newArray)
    }
};

export const handel_task = (data) => {
    console.log('imdata',data);
     return {
        type: GET_TASK,
        payload:data
    };
};

  const add_task = (data) => {
    return {
        type: ADD_TASK,
        payload:data
    };
};



const taskUpdated = (data) => {
    return {
        type: UPDATE_TASK,
        payload:data
    };
};  

const taskDelete = (data) => {
    return {
        type: DELETE_TASK,
        payload:data
    };
};  



const saveTaskInLocalStorage = (data) => {
    localStorage.setItem('tasks', JSON.stringify(data));
};   

export const SnackData=(data)=>{
    return {
        type: SNACK,
        payload:data
    };
}

export const update_task = (data,index) => {
    return (dispatch,getstate)=> {
        let newArray = [...getstate().TaskList.Tasks]
        newArray[index] =  data
        dispatch(taskUpdated(newArray))
        saveTaskInLocalStorage(newArray)
        dispatch(SnackData({
            open: true,
            message: 'update dont !',
            type: 'success'
        }))
    }
};
 
export const deleteTask = (index) => {
    return (dispatch,getstate)=> {
        let newArray =  [...getstate().TaskList.Tasks]
        newArray.splice(index, 1);
        dispatch(taskDelete(newArray))
        saveTaskInLocalStorage(newArray)
        dispatch(SnackData({
            open: true,
            message: 'delete dont !',
            type: 'error'
        }))
    }
};